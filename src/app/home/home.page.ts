import {Component, OnInit} from '@angular/core';
import {AlertController} from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  currentValue = 0;
  offlineStore: Storage;

  constructor(public alertController: AlertController) {
  }

  increment() {
    this.currentValue++;
    this.offlineStore.setItem('count', this.currentValue.toString());
  }

  ngOnInit() {
    this.offlineStore = window.localStorage;
    this.currentValue = parseInt(localStorage.getItem('count'), 10);
    this.currentValue = Number.isNaN(this.currentValue) ? 0 : this.currentValue;
  }

  // reset() {
  //   if (confirm('Are you sure to reset ??')) {
  //     this.currentValue = 0;
  //   }
  // }
  async reset() {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: `Are you sure to reset ??`,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          },
        },
        {
          text: 'Okay',
          handler: () => {
            this.currentValue = 0;
            this.offlineStore.setItem('count', this.currentValue.toString());

          },
        },
      ],
    });
    await alert.present();
  }
}
