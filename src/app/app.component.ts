import {Component, OnInit} from '@angular/core';
import {Insomnia} from '@awesome-cordova-plugins/insomnia/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private insomnia: Insomnia) {
  }

  ngOnInit() {
    this.sleep();
  }

  sleep() {
    this.insomnia.keepAwake()
      .then(
        () => console.log('success'),
        () => console.log('error')
      );
    // this.insomnia.allowSleepAgain()
    //   .then(
    //     () => console.log('success'),
    //     () => console.log('error')
    //   );
  }
}
